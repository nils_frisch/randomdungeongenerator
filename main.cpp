#include <SFML/Graphics.hpp>
#include <random>
#include <vector>
#include <time.h>
#include <iostream>
#include <math.h>

const double epsilon = 0.0001;
struct Room
{
    sf::Vector2f position;
    sf::Vector2u size;
    sf::Color color;
    sf::Vector2f separationVector;
};

struct Circle
{
    sf::Vector2f center;
    double radius;
};

struct Edge
{
    sf::Vector2f Node1;
    sf::Vector2f Node2;
    Edge(sf::Vector2f node1, sf::Vector2f node2):Node1(node1),Node2(node2) {}
    Edge() {}
};
struct NodeList
{
    std::vector<sf::Vector2f> openNodes;
    std::vector<sf::Vector2f> closedNodes;
};

struct Triangle
{
    Edge edge1;
    Edge edge2;
    Edge edge3;
};
std::minstd_rand0 generator(time(NULL));



inline double vectorMagnitude(sf::Vector2f a);
inline double crossProduct2D(sf::Vector2f a, sf::Vector2f b);
double getNormalDistributedNumber();
double getEdgeLength(Edge edge);

std::vector<Room> clearRooms(const std::vector<Room> &rooms, const std::vector<Edge> &corridors, sf::Color keepColor);
std::vector<Room> generateRooms(sf::Vector2u levelSize, int length, float maxRatioDifference, int n);
std::vector<Edge> delauneyGraph(std::vector<sf::Vector2f> nodes);
std::vector<Edge> bruteForceDelaunay(std::vector<sf::Vector2f> nodes);
std::vector<Edge> eliminateDuplicates(const std::vector<Edge> &edges);
std::vector<Edge> makeTree(std::vector<Edge> &treeEdges, std::vector<Edge> &sortedEdges, std::vector<sf::Vector2f> &vNew, int nodeAmount);
std::vector<Edge> sortEdgesByLength(std::vector<Edge> edges);
std::vector<Edge> makeMinimumSpanningTree(std::vector<Edge> edges, int nodeAmount);
std::vector<Edge> spliceEdges(const std::vector<Edge> &a, const std::vector<Edge> &b, int amount);
std::vector<Edge> makeLShapeCorridor(sf::Vector2f startPosition, sf::Vector2f endPosition, bool horizontalStart);
std::vector<Edge> makeLShapeCorridor(Room room1, Room room2,sf::Vector2f vec, bool horizontal, const std::vector<Room> &rooms);
std::vector<Edge> createCorridors(const std::vector<Room> &rooms, const std::vector<Edge> &edges);
std::vector<Edge> makeCorridor(Room room1, Room room2, const std::vector<Room> &rooms);
std::vector<Edge> makeStraightCorridor (Room room1, Room room2, const std::vector<Room> rooms);

int pointsInCircle(Circle circle, const std::vector<sf::Vector2f> &nodes);

bool areRectanglesIntersecting(sf::Vector2f pos1, sf::Vector2f halfSize1,sf::Vector2f pos2, sf::Vector2f halfSize2 );
bool areRoomsIntersecting(Room roomA, Room roomB);
bool isInCircle(sf::Vector2f p, Circle circle);
bool seperateBoxes(std::vector<Room> &boxes, float velocity);
bool meetsDelauneyCondition(Circle circle, const NodeList &nodes);
bool meetsDelauneyCondition(Circle circle, const std::vector<sf::Vector2f> &nodes);
bool isIntersecting(Edge edge1, Edge edge2);
bool areEdgesIntersecting(Edge edge, const std::vector<Edge> &openEdges, const std::vector<Edge> &closedEdges);
bool edgeIsInVector(Edge edge, std::vector<Edge> l);
bool areEqual(Edge e1, Edge e2);
bool addsToTree(Edge e,const std::vector<sf::Vector2f> &l);
bool pointInVector(sf::Vector2f p,const std::vector<sf::Vector2f> &l);
bool isInRoom(sf::Vector2f point, Room room);
bool vectorIntersectsRoom(sf::Vector2f origin, sf::Vector2f vec, Room room);
bool isLegalPath(const Edge &edge, const std::vector<Room> &rooms);
bool areLegalPaths(const std::vector<Edge> &paths, const std::vector<Room> &rooms);

void findNextNode(std::vector<sf::Vector2f> nodes, std::vector<Edge> &openEdges, std::vector<Edge> &closedEdges);
void drawEdge(sf::RenderWindow &window, Edge edge, float thickness, sf::Color color);

sf::Vector2f normalize(sf::Vector2f v);
sf::Vector2f calculateIntersectionPoint(sf::Vector2f a, sf::Vector2f av, sf::Vector2f b, sf::Vector2f bv);
inline sf::Vector2f crossProduct2D1V(sf::Vector2f a);

Circle calculateCircumCirle(sf::Vector2f a, sf::Vector2f b, sf::Vector2f c);
Room findRoom(sf::Vector2f position, const std::vector<Room> &rooms);

int main()
{
    sf::RenderWindow window(sf::VideoMode(400  , 400), "Generator");

    std::vector<Room> rooms;
    std::vector<Room> roomsDisplayed;
    std::vector<Edge> delauneyEdges;
    std::vector<Edge> mstEdges;
    std::vector<Edge> displayedEdges;
    std::vector<Edge> splicedEdges;
    std::vector<sf::Vector2f> nodes;
    std::vector<Edge> corridors;

    bool seperated = true;
    uint64_t seperationIterations = 0;
    while(window.isOpen())
    {
        sf::Event event;
        while(window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                window.close();
            }
            if (event.type == sf::Event::KeyPressed)
            {
                if (event.key.code == sf::Keyboard::Escape) window.close();
                if (event.key.code == sf::Keyboard::Space)
                {
                    rooms = generateRooms(sf::Vector2u(400,400), 40, 0.5, 150);
                    roomsDisplayed = rooms;
                    seperated = false;
                    seperationIterations = 0;
                    delauneyEdges.clear();
                    mstEdges.clear();
                    displayedEdges.clear();
                    splicedEdges.clear();
                    nodes.clear();
                    corridors.clear();
                }
                if (event.key.code == sf::Keyboard::L)
                {
                    for (int i = 0; i < rooms.size(); i++)
                    {
                        std::cout << "B" << i << " W:" << rooms[i].size.x << " H:" << rooms[i].size.y
                                  << " X:" << rooms[i].position.x << " Y:" << rooms[i].position.y << std::endl;
                    }
                }
                if (event.key.code == sf::Keyboard::E)
                {
                    if (nodes.size() > 0)
                    {
                        delauneyEdges = delauneyGraph(nodes);
                        std::cout << "Nodes Checked:" << nodes.size() <<" Edges found:" << delauneyEdges.size() << std::endl;
                        displayedEdges = delauneyEdges;
                    }
                }
                if (event.key.code == sf::Keyboard::B)
                {

                    if (nodes.size() > 0)
                    {
                        delauneyEdges = bruteForceDelaunay(nodes);
                        std::cout << "Nodes Checked:" << nodes.size() <<" Edges found:" << delauneyEdges.size() << std::endl;
                        displayedEdges = delauneyEdges;
                    }
                }
                if (event.key.code == sf::Keyboard::M)
                {
                    mstEdges = makeMinimumSpanningTree(delauneyEdges, nodes.size());
                    std::cout << mstEdges.size() << " of " << displayedEdges.size() << " left after makeMinimumSpanningTree()" << std::endl;
                    displayedEdges = mstEdges;
                }
                if (event.key.code == sf::Keyboard::Comma)
                {
                    int amountToSplice = (delauneyEdges.size()-mstEdges.size())/4;
                    splicedEdges = spliceEdges(delauneyEdges, mstEdges, amountToSplice);
                    std::cout << splicedEdges.size() - displayedEdges.size() << " added after spliceEdges()" << std::endl;
                    displayedEdges = splicedEdges;
                }
                if (event.key.code == sf::Keyboard::R)
                {
                    double m=0;
                    for (int i = 0; i < 10000 ; i++)
                    {
                        float mt = getNormalDistributedNumber();
                        if (fabs(mt) > m)
                            m = fabs(mt);
                    }
                    std::cout << "max NormalDistributionNumber Generated (out of 10000):" << m <<std::endl;
                }
                if (event.key.code == sf::Keyboard::C)
                {
                    corridors = createCorridors(rooms, displayedEdges);
                }
                if (event.key.code == sf::Keyboard::X)
                {
                    roomsDisplayed = clearRooms(roomsDisplayed, corridors, sf::Color(211,0,0,255));
                    displayedEdges.clear();
                }
                if (event.key.code == sf::Keyboard::Y)
                {
                    roomsDisplayed = rooms;
                    displayedEdges = splicedEdges;
                }

            }
        }
        if (!seperated)
        {
            seperated = seperateBoxes(rooms, 1);
            seperationIterations++;
            if (seperated)
            {
                std::cout << "Seperation Iterations:" << seperationIterations << std::endl;
                for (int i = 0; i < rooms.size(); i++)
                {
                    if (rooms[i].color == sf::Color(211,0,0,255))
                    {
                        nodes.push_back(rooms[i].position);
                    }
                }

            }
            roomsDisplayed = rooms;
        }
        window.clear();
        for (int i = 0; i < roomsDisplayed.size(); i++)
        {
            sf::RectangleShape rectShape;
            rectShape.setSize(sf::Vector2f(roomsDisplayed[i].size.x*2, roomsDisplayed[i].size.y*2));
            rectShape.setOrigin(roomsDisplayed[i].size.x, roomsDisplayed[i].size.y);
            rectShape.setPosition(sf::Vector2f(roomsDisplayed[i].position.x,roomsDisplayed[i].position.y));
            rectShape.setFillColor(roomsDisplayed[i].color);
            rectShape.setOutlineColor(sf::Color(255,128,128,255));
            rectShape.setOutlineThickness(1);
            window.draw(rectShape);
        }
        for (int i = 0; i < displayedEdges.size(); i++)
        {
            drawEdge(window, displayedEdges[i], 1, sf::Color::Green);
        }
        for (int i = 0; i < corridors.size(); i++)
        {
            drawEdge(window, corridors[i], 1, sf::Color::Yellow);
        }

        window.display();
    }
    return 0;

}

std::vector<Room> generateRooms(sf::Vector2u levelSize, int maxLength, float maxRatioDifference, int n)
{
    double maxRatio = 0;
    double minRatio = 100;
    int aboveThreshhold = 0;
    std::vector<Room> rooms;
    for (int i = 0; i < n ; i++ )
    {
        double width = (maxLength * getNormalDistributedNumber());
        double maxLengthDiff = (width / 1-maxRatioDifference) - (width / 1 + maxRatioDifference);
        double height = (width - maxLengthDiff)+ (rand() % (int)(2 * maxLengthDiff));

        //    std::cout << "W:" << width << " H:" << height << " W/H:" << width/height << " 1-maxR:"
        //                << 1-maxRatioDifference << " 1+maxR:" << 1+maxRatioDifference << std::endl;
        Room room;
        room.size = sf::Vector2u((int)height/2,(int) width/2);
        room.position = sf::Vector2f(rand()% (levelSize.x/8) + (levelSize.x/2-((levelSize.x/8/2))),
                                     rand()%(levelSize.y/8)+(levelSize.y/2-((levelSize.x/8)/2)));
        room.color = sf::Color(0, 0, 211, 255);

        if (maxRatio < width/height)
            maxRatio = width/height;
        if (minRatio > width/height)
            minRatio = width/height;
        if (width > (maxLength * 0.9) || height > (maxLength * 0.9))
        {
            aboveThreshhold++;
            room.color = sf::Color(211,0,0,255);
        }

        rooms.push_back(room);
    }
    std::cout << "MaxRatio:" << maxRatio << " MinRatio:" << minRatio << " Above 80% length:" << aboveThreshhold <<std::endl;

    return rooms;
}


double getNormalDistributedNumber()
{
    std::normal_distribution<double> distribution;
    double res = distribution(generator);
    //while (res > 1) res /= 2;
    return fabs(res/2);
}

bool seperateBoxes(std::vector<Room> &boxes, float velocity)
{
    bool separated = true;
    for (int i = 0; i < boxes.size(); i++)
    {
        sf::Vector2f v(0,0);
        for(int j = 0; j < boxes.size(); j++)
        {
            if(i==j)
                continue;
            if (areRoomsIntersecting(boxes[i], boxes[j]))
            {
                v += boxes[i].position - boxes[j].position;
                separated = false;
            }
        }
        boxes[i].separationVector = normalize(v);
    }
    for (int i = 0; i < boxes.size(); i++)
    {
        boxes[i].position  += (boxes[i].separationVector * (float)velocity);
    }
    return separated;
}

bool areRoomsIntersecting(Room roomA, Room roomB)
{
    return  areRectanglesIntersecting(roomA.position, sf::Vector2f(roomA.size), roomB.position, sf::Vector2f(roomB.size));
}

bool areRectanglesIntersecting(sf::Vector2f pos1, sf::Vector2f halfSize1,sf::Vector2f pos2, sf::Vector2f halfSize2 )
{
    return !((pos1.x+halfSize1.x < pos2.x-halfSize2.x) ||
              (pos1.x-halfSize1.x > pos2.x+halfSize2.x) ||
              (pos1.y+halfSize1.y < pos2.y-halfSize2.y) ||
              (pos1.y-halfSize1.y > pos2.y+halfSize2.y));
}

sf::Vector2f normalize(sf::Vector2f v)
{
    if (!v.x && !v.y)
        return v;
    double c = vectorMagnitude(v);
    v /= (float)c;
    return v;
}

Circle calculateCircumCirle(sf::Vector2f a, sf::Vector2f b, sf::Vector2f c)
{
    sf::Vector2f ab = b-a;
    sf::Vector2f bc = c-b;

    sf::Vector2f abM = a+(ab/(float)2.0);
    sf::Vector2f bcM = b+(bc/(float)2.0);

    sf::Vector2f abMPerp = normalize(crossProduct2D1V(ab));
    sf::Vector2f bcMPerp = normalize(crossProduct2D1V(bc));

    sf::Vector2f pCenter = calculateIntersectionPoint(abM, abMPerp, bcM, bcMPerp);
    double r = vectorMagnitude(pCenter-a);

    Circle res;
    res.radius = r;
    res.center = pCenter;

    return res;
}

sf::Vector2f calculateIntersectionPoint(sf::Vector2f a, sf::Vector2f av, sf::Vector2f b, sf::Vector2f bv)
{
    double t = crossProduct2D(a-b, av) / crossProduct2D(bv,av);
    return b + (float) t * bv;
}

inline double crossProduct2D(sf::Vector2f a, sf::Vector2f b)
{
    return a.x*b.y - a.y*b.x;
}

inline double vectorMagnitude(sf::Vector2f a)
{
    return sqrt(pow(a.x,2)+pow(a.y,2));
}

inline sf::Vector2f crossProduct2D1V(sf::Vector2f a)
{
    return sf::Vector2f(a.y * (float)-1, a.x);
}

bool isInCircle(sf::Vector2f p, Circle circle)
{
    double pcv = vectorMagnitude(p-circle.center);
    return (pcv - circle.radius) < epsilon;
}

std::vector<Triangle> findAllDelaunayTriangles(sf::Vector2f startNode, const std::vector<sf::Vector2f> &nodes)
{
    std::vector<Triangle> res;
    for (int i = 0; i < nodes.size(); i++)
    {
        if (nodes[i] == startNode)
            continue;
        for (int j = 0; j < nodes.size(); j++)
        {
            if (nodes[j] == startNode || j==i)
                continue;
            Circle circle = calculateCircumCirle(startNode, nodes[i], nodes[j]);
            if (meetsDelauneyCondition(circle, nodes))
            {
                //found a good one, lets turn it to edges
                Triangle triangle;
                triangle.edge1 = Edge(startNode, nodes[i]);
                triangle.edge2 = Edge(startNode, nodes[j]);
                triangle.edge3 = Edge(nodes[j], nodes[i]);
                res.push_back(triangle);
            }
        }
    }
    return res;
}

std::vector<Edge> bruteForceDelaunay(std::vector<sf::Vector2f> nodes)
{
    std::vector<Edge> edges;
    std::vector<Triangle> triangles;
    for (int i = 0; i < nodes.size(); i++)
    {
        std::vector<Triangle> t = findAllDelaunayTriangles(nodes[i], nodes);
        triangles.insert(triangles.end(), t.begin(), t.end());
    }
    for (int i = 0; i < triangles.size(); i++)
    {
        edges.push_back(triangles[i].edge1);
        edges.push_back(triangles[i].edge2);
        edges.push_back(triangles[i].edge3);
    }

    edges = eliminateDuplicates(edges);

    return edges;
}
std::vector<Edge> eliminateDuplicates(const std::vector<Edge> &edges)
{
    std::vector<Edge> res;
    for (int i = 0; i < edges.size(); i++)
    {
        if (edgeIsInVector(edges[i], res))
            continue;
        res.push_back(edges[i]);
    }
    return res;
}

bool edgeIsInVector(Edge edge, std::vector<Edge> l)
{
    for (int i = 0; i < l.size(); i++)
    {
        if(areEqual(edge, l[i]))
            return true;
    }
    return false;
}

bool areEqual(Edge e1, Edge e2)
{
    return ((e1.Node1 == e2.Node1 && e1.Node2 == e2.Node2 ) || (e1.Node1 == e2.Node2 && e1.Node2 == e2.Node1));
}


std::vector<Edge> findFirstTriangle(NodeList &nodeList)
{
    sf::Vector2f startNode = nodeList.openNodes.back();
    nodeList.openNodes.pop_back();
    nodeList.closedNodes.push_back(startNode);
    std::vector<Edge> edges;

    for (int i = 0; i < nodeList.openNodes.size(); i++)
    {
        for (int j = 0; j < nodeList.openNodes.size(); j++)
        {
            if (i==j)
                continue;
            Circle circle = calculateCircumCirle(startNode, nodeList.openNodes[i], nodeList.openNodes[j]);
            if (meetsDelauneyCondition(circle, nodeList))
            {
                std::cout << "FOUND ONE" << std::endl;
                edges.push_back(Edge(startNode, nodeList.openNodes[i]));
                edges.push_back(Edge(startNode, nodeList.openNodes[j]));
                edges.push_back(Edge(nodeList.openNodes[j], nodeList.openNodes[i]));
                nodeList.closedNodes.push_back(nodeList.openNodes[i]);
                nodeList.closedNodes.push_back(nodeList.openNodes[j]);
                if (i > j)
                {
                    nodeList.openNodes.erase(nodeList.openNodes.begin()+i);
                    nodeList.openNodes.erase(nodeList.openNodes.begin()+j);
                }
                else
                {
                    nodeList.openNodes.erase(nodeList.openNodes.begin()+j);
                    nodeList.openNodes.erase(nodeList.openNodes.begin()+i);
                }
                return edges;

            }
        }
    }
}

int pointsInCircle(Circle circle, const std::vector<sf::Vector2f> &nodes)
{
    int counter = 0;
    for (int i = 0; i < nodes.size(); i++)
    {
        if (isInCircle(nodes[i], circle))
            counter++;
    }
    return counter;
}
bool meetsDelauneyCondition(Circle circle, const std::vector<sf::Vector2f> &nodes)
{
    return pointsInCircle(circle, nodes) <= 3;
}

bool meetsDelauneyCondition(Circle circle, const NodeList &nodes)
{
    return pointsInCircle(circle, nodes.closedNodes) + pointsInCircle(circle, nodes.openNodes) <= 3;
}

std::vector<Edge> delauneyGraph(std::vector<sf::Vector2f> nodes)
{
    NodeList nodeList;
    nodeList.openNodes = nodes;
    std::cout << "checking " << nodeList.openNodes.size() << " Nodes..." << std::endl;
    std::vector<Edge> openEdges = findFirstTriangle(nodeList);
    std::cout << "found " << openEdges.size() << " Edges" << std::endl;
    std::vector<Edge> closedEdges;
    findNextNode(nodes, openEdges, closedEdges);
    closedEdges.insert(closedEdges.end(), openEdges.begin(), openEdges.end());
    return closedEdges;

}

void findNextNode(std::vector<sf::Vector2f> nodes, std::vector<Edge> &openEdges, std::vector<Edge> &closedEdges)
{
    bool newEdge = false;
    Edge edge = openEdges.back();
    openEdges.pop_back();
    NodeList nlist;
    nlist.openNodes = nodes;
    for (int i = 0; i < nodes.size(); i++)
    {
        Circle circumCircle = calculateCircumCirle(edge.Node1, edge.Node2, nodes[i]);
        if (meetsDelauneyCondition(circumCircle, nlist))
        {
            Edge e1 = Edge(edge.Node1, nodes[i]);
            Edge e2 = Edge(edge.Node2, nodes[i]);
            openEdges.push_back(e1);
            openEdges.push_back(e2);
            newEdge = true;
            break;
        }
    }
    closedEdges.push_back(edge);
    if (!newEdge || openEdges.size() == 0)
        findNextNode(nodes, openEdges, closedEdges);
    /*
        for (int i = 0 ; i < nodeList.openNodes.size();i++)
        {
            for (int j = 0; j < openEdges.size(); j++)
            {
                Circle circumCircle = calculateCircumCirle(openEdges[j].Node1, openEdges[j].Node2, nodeList.openNodes[i]);
                if (meetsDelauneyCondition(circumCircle, nodeList))
                {
                    Edge e1 = Edge(openEdges[j].Node1, nodeList.openNodes[i]);
                    Edge e2 = Edge(openEdges[j].Node2, nodeList.openNodes[i]);
                    if (areEdgesIntersecting(e1, openEdges, closedEdges) || areEdgesIntersecting(e2, openEdges, closedEdges))
                        continue;
                    openEdges.push_back(e1);
                    openEdges.push_back(e2);
                    closedEdges.push_back(openEdges[j]);
                    openEdges.erase(openEdges.begin()+j);
                    nodeList.closedNodes.push_back(nodeList.openNodes[i]);
                    nodeList.openNodes.erase(nodeList.openNodes.begin()+i);

                    findNextNode(nodeList, openEdges, closedEdges);

                    return;
                }
            }
        }
    */
}

std::vector<Edge> sortEdgesByLength(std::vector<Edge> edges)
{
    std::vector<Edge> res;


    while(edges.size())
    {
        int biggest = 0;
        double biggestSize = 0;
        for (int i = 0; i < edges.size(); i++)
        {
            double eSize = getEdgeLength(edges[i]);
            if (eSize > biggestSize)
            {
                biggestSize = eSize;
                biggest = i;
            }
        }
        res.push_back(edges[biggest]);
        edges.erase(edges.begin()+biggest);
    }
    return res;
}

std::vector<Edge> makeMinimumSpanningTree(std::vector<Edge> edges, int nodeAmount)
{
    std::vector<sf::Vector2f> vNew;
    std::vector<Edge> treeEdges;
    std::vector<Edge> sortedEdges = sortEdgesByLength(edges);
    if (sortedEdges.size() != edges.size())
        std::cout << " sorted Edges:" << sortedEdges.size() << " original Edges:"<<edges.size() << std::endl;
    Edge e = sortedEdges.back();
    sortedEdges.pop_back();
    vNew.push_back(e.Node1);
    vNew.push_back(e.Node2);
    treeEdges.push_back(e);
    return makeTree(treeEdges, sortedEdges, vNew, nodeAmount);
}

std::vector<Edge> makeTree(std::vector<Edge> &treeEdges, std::vector<Edge> &sortedEdges, std::vector<sf::Vector2f> &vNew, int nodeAmount)
{
    if (vNew.size() == nodeAmount)
        return treeEdges;
    for (int i = sortedEdges.size()-1; i >= 0; i--)
    {
        if(addsToTree(sortedEdges[i], vNew))
        {
            treeEdges.push_back(sortedEdges[i]);
            if (pointInVector(sortedEdges[i].Node1, vNew))
                vNew.push_back(sortedEdges[i].Node2);
            else
                vNew.push_back(sortedEdges[i].Node1);
            sortedEdges.erase(sortedEdges.begin()+i);
            return makeTree(treeEdges, sortedEdges, vNew, nodeAmount);
        }
    }
    return treeEdges;
}

bool addsToTree(Edge e,const std::vector<sf::Vector2f> &l)
{
    return (pointInVector(e.Node1,l) && !pointInVector(e.Node2,l)) || (pointInVector(e.Node2,l) && !pointInVector(e.Node1,l));
}

bool pointInVector(sf::Vector2f p,const std::vector<sf::Vector2f> &l)
{
    for(int i = 0; i < l.size(); i++)
    {
        if(p == l[i])
            return true;
    }
    return false;
}
void drawEdge(sf::RenderWindow &window, Edge edge, float thickness, sf::Color color)
{
    sf::Vector2f nodeV = edge.Node1 - edge.Node2;
    sf::Vector2f nodeVPN = normalize(crossProduct2D1V(nodeV));
    sf::ConvexShape cShape(4);
    cShape.setPoint(0, edge.Node1 + -thickness*nodeVPN);
    cShape.setPoint(1, edge.Node1 + thickness*nodeVPN);
    cShape.setPoint(2, edge.Node2 + thickness*nodeVPN);
    cShape.setPoint(3, edge.Node2 + -thickness*nodeVPN);
    cShape.setFillColor(color);
    window.draw(cShape);
}

bool isIntersecting(Edge edge1, Edge edge2)
{
    sf::Vector2f v1 = edge1.Node1 - edge1.Node2;
    sf::Vector2f v2 = edge2.Node1 - edge2.Node2;

    sf::Vector2f intersectP = calculateIntersectionPoint(edge1.Node2, v1, edge2.Node2, v2);

    double v1scalar = vectorMagnitude(v1);
    double v2scalar = vectorMagnitude(v2);

    sf::Vector2f v1_ = intersectP - edge1.Node2;
    sf::Vector2f v2_ = intersectP - edge2.Node2;

    double v1_scalar = vectorMagnitude(v1_);
    double v2_scalar = vectorMagnitude(v2_);

    if((v1scalar > 0 && v1scalar >= v1_scalar) && (v2scalar > 0 && v2scalar >= v2_scalar))
        return true;

    if((v1scalar < 0 && v1scalar <= v1_scalar) && (v2scalar < 0 && v2scalar <= v2_scalar))
        return true;
    return false;
}

//Temporary nullified;
bool areEdgesIntersecting(Edge edge, const std::vector<Edge> &openEdges, const std::vector<Edge> &closedEdges)
{
    return false;
    for(int i = 0 ; i < (closedEdges.size() > openEdges.size() ? closedEdges.size() : openEdges.size()); i++)
    {
        if ((i < closedEdges.size() && isIntersecting(edge, closedEdges[i])) || (i < openEdges.size() && isIntersecting(edge, openEdges[i])))
            return true;
    }

}

double getEdgeLength(Edge edge)
{
    return fabs(vectorMagnitude(edge.Node1-edge.Node2));
}
//splices _amount_ edges from a into b;
std::vector<Edge> spliceEdges(const std::vector<Edge> &source, const std::vector<Edge> &target, int amount)
{
    std::vector<Edge> aSorted = sortEdgesByLength(source);
    std::vector<Edge> res = target;
    for (int i = 0; i < amount;)
    {
        int index = rand() % (aSorted.size() - aSorted.size()/4) + aSorted.size()/4;
        if (!edgeIsInVector(aSorted[index], res))
        {
            i++;
            res.push_back(aSorted[index]);
        }
    }
    return res;

}

bool isInRoom(sf::Vector2f point, Room room)
{
    return  (point.x > (room.position.x - room.size.x)) &&
            (point.x < (room.position.x + room.size.x)) &&
            (point.y > (room.position.y - room.size.y)) &&
            (point.y < (room.position.y + room.size.y)) ;
}

//vector vec should be a Horizontal or Vertical vector
bool vectorIntersectsRoom(sf::Vector2f origin, sf::Vector2f vec, Room room)
{
    if(fabs(vec.x) < epsilon)
    {
        if (fabs(origin.x - room.position.x) > room.size.x )
            return false;

        return (origin.y < room.position.y && origin.y+vec.y > room.position.y + room.size.y) ||
                (origin.y > room.position.y && origin.y+vec.y < room.position.y - room.size.y);
    }
    else if (fabs(vec.y) < epsilon)
    {

        if (fabs(origin.y - room.position.y) > room.size.y )
            return false;

        return (origin.x < room.position.x && origin.x+vec.x > room.position.x + room.size.x) ||
                (origin.x > room.position.x && origin.x+vec.x < room.position.x - room.size.x);
    }
    return false;
}

std::vector<Edge> createCorridors(const std::vector<Room> &rooms, const std::vector<Edge> &edges)
{
    std::vector<Edge> corridors;
    for (const Edge &edge: edges)
    {
        sf::Vector2f eV = edge.Node2 - edge.Node1;
        sf::Vector2f startPosition;
        sf::Vector2f endPosition;
        bool horizontal;
        Room room1 = findRoom(edge.Node1, rooms);
        Room room2 = findRoom(edge.Node2, rooms);

        std::vector<Edge> corridor = makeCorridor(room1, room2, rooms);
        corridors.insert(corridors.end(), corridor.begin(), corridor.end());
    }
    return corridors;
}

Room findRoom(sf::Vector2f position, const std::vector<Room> &rooms)
{
    for(const Room &room : rooms)
    {
        if (room.position == position)
            return room;
    }
}

std::vector<Edge> makeCorridor(Room room1, Room room2, const std::vector<Room> &rooms)
{
    sf::Vector2f vec = room2.position-room1.position;
    bool horizontalFirst = (fabs(vec.x) > fabs(vec.y));
    std::vector<Edge> res = makeLShapeCorridor(room1, room2, vec, horizontalFirst, rooms);
    if (res.size() == 0)
        res = makeLShapeCorridor(room1,room2, vec, !horizontalFirst, rooms);

    if (res.size() == 0)
        res = makeStraightCorridor(room1, room2, rooms);


    return res;

}

std::vector<Edge> makeLShapeCorridor(Room room1, Room room2,sf::Vector2f vec, bool horizontal, const std::vector<Room> &rooms)
{
    sf::Vector2f vecD= sf::Vector2f(vec.x/fabs(vec.x), vec.y/fabs(vec.y) );
    sf::Vector2f startP = horizontal ? sf::Vector2f(room1.position.x + (room1.size.x * vecD.x), room1.position.y) : sf::Vector2f(room1.position.x,room1.position.y + (room1.size.y * vecD.y));
    sf::Vector2f endP = horizontal ? sf::Vector2f(room2.position.x, room2.position.y + (room2.size.y * (vecD.y * -1))) : sf::Vector2f(room2.position.x + (room2.size.x * (vecD.x * -1)),room2.position.y);

    std::vector<Edge> res = makeLShapeCorridor(startP, endP, horizontal);
    if(!areLegalPaths(res, rooms))
        res.clear();

    return res;
}

bool areLegalPaths(const std::vector<Edge> &paths, const std::vector<Room> &rooms)
{
  for (const Edge path : paths)
  {
      if (!isLegalPath(path, rooms))
        return false;
  }
  return true;
}

bool isLegalPath(const Edge &edge, const std::vector<Room> &rooms)
{
    for (const Room room: rooms)
    {
         if (room.color != sf::Color(211,0,0,255))
            continue;
        if(isInRoom(edge.Node1, room) || isInRoom(edge.Node2, room) || vectorIntersectsRoom(edge.Node1,edge.Node2-edge.Node1, room))
            return false;
    }
    return true;
}

Edge makeStraightCorridor(sf::Vector2f startP, sf::Vector2f endP)
{

    return Edge(startP, endP);
}

std::vector<Edge> makeStraightCorridor (Room room1, Room room2, const std::vector<Room> rooms)
{
    Edge path;
    if (fabs(room1.position.y - room2.position.y) < room2.size.y)
    {
        sf::Vector2f startP = room1.position + sf::Vector2f((float)room1.size.x * (room1.position.x > room2.position.x ? -1 : 1),0);
        sf::Vector2f endP = sf::Vector2f(room2.position.x, room1.position.y) + sf::Vector2f((float)room2.size.x * (room1.position.x > room2.position.x ? 1 : -1),0);
        path = makeStraightCorridor(startP, endP);
    }
    else if (fabs(room1.position.x - room2.position.x) < room2.size.x)
    {
        sf::Vector2f startP = room1.position + sf::Vector2f(0, (float)room1.size.y * (room1.position.y > room2.position.y ? -1 : 1));
        sf::Vector2f endP = sf::Vector2f(room1.position.x, room2.position.y) + sf::Vector2f(0,(float)room2.size.y * (room1.position.y > room2.position.y ? 1 : -1));
        path = makeStraightCorridor(startP, endP);
    }
    std::vector<Edge> res;
    if (isLegalPath(path, rooms))
    {
        res.push_back(path);
    }
    return res;
}

std::vector<Edge> makeLShapeCorridor(sf::Vector2f startPosition, sf::Vector2f endPosition, bool horizontalStart)
{
    sf::Vector2f midPoint;
    std::vector<Edge> res;

    if( horizontalStart)
        midPoint = sf::Vector2f(endPosition.x, startPosition.y);
    else
        midPoint = sf::Vector2f(startPosition.x, endPosition.y);

    res.push_back(Edge(startPosition, midPoint));
    res.push_back(Edge(midPoint, endPosition));
    return res;
}

std::vector<Room> clearRooms(const std::vector<Room> &rooms, const std::vector<Edge> &corridors, sf::Color keepColor)
{
    std::vector<Room> res;
    for (const Room &room : rooms)
    {
        if(room.color == keepColor)
        {
            res.push_back(room);
            continue;
        }
        for(const Edge &path : corridors)
        {
            if (vectorIntersectsRoom(path.Node1,path.Node2-path.Node1, room) || isInRoom(path.Node1, room) || isInRoom(path.Node2, room) )
            {
                res.push_back(room);
                std::cout << "Edge (" << path.Node1.x << "|" << path.Node1.y << "), (" << path.Node2.x << "|" << path.Node2.y << ") intersects Room ("
                            << room.position.x << "|"<< room.position.y << ") " << "(" << room.size.x << "|" << room.size.y << ")"<< std::endl;
                break;

            }
        }
    }
    return res;
}
